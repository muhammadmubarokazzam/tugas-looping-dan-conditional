<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Looping PHP</h1>
    <?php
    echo "<h3> Soal No 1 : Looping I Love PHP</h3>";
    echo "<h5>LOOPING PERTAMA</h5>";
    for ($i=2; $i<=20; $i+=2){
        echo $i ." - I Love PHP <br>";
    }
    echo "<h5>LOOPING KEDUA</h5>";
    for ($a=20; $a>=2; $a-=2){
        echo $a . " - I Love PHP <br>";
    }


    echo "<h3> Soal No 2 : Looping Array Module</h3>";
    $numbers = [18,45,29,61,47,34];
    echo "Array angka : ";
    print_r($numbers);
    echo "<br>";
    echo "Array sisa baginya adalah:  ";
    echo "<br>";
    foreach($numbers as $value){
        $rest[] = $value %=5;
    }
    echo "<br>";
    print_r($rest);
    echo "<br>";


    echo "<h3> Soal No 3 : Looping Asociative Array</h3>";
    $biodata = [
        ["001", "Keyboard Logitek", "60000", "Keyboard yang mantap untuk kantoran", "logitek.jpeg"],
        ["002", "Keyboard MSI", "300000", "Keyboard gaming MSI mekanik", "msi.jpeg"],
        ["003", "Mouse Genius", "50000", "Mouse Genius biar lebih pinter", "genius.jpeg"],
        ["004", "Mouse Jerry", "30000", "Mouse yang disukai kucing", "jerry.jpeg"],
    ];

    foreach($biodata as $key => $hasil){
        $item = array(
            "id" => $hasil[0],
            "name" => $hasil[1],
            "price" => $hasil[2],
            "description" => $hasil[3],
            "source" => $hasil[4],
        );
        echo "<br>";
        print_r($item);
        echo "<br>";
    }

    echo "<h3>Soal No 4 : Asterix</h3>";
    for ($k = 1; $k<=5; $k++){
        for ($m = 1; $m<=$k; $m++){
            echo "*";
        }
        echo "<br>";
    }
    ?>
</body>
</html>